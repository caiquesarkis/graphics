const aspectRatio =  window.innerWidth / window.innerHeight
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, aspectRatio, 0.1, 1000 );
const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
let amplitude = document.getElementById("Amplitude").value
let lambda = document.getElementById("Lambda").value
let range = 10;
let startPoint = 100;
let resolution = 0.002;
let phase = 0;
let waveLength = 10;
let waveVelocity = 0.1;

camera.position.set( 0, 0, 100 );
camera.lookAt( 0, 0, 0 );

const material = new THREE.LineBasicMaterial( { color: 0x856661, linewidth:1} );

const points = [];


for (i=0 ; i < range ; i+=resolution){
    points.push( new THREE.Vector3( waveLength*i-startPoint, amplitude*Math.sin(lambda*i + phase), 0 ) );
}

const geometry = new THREE.Geometry();
geometry.vertices = points

const line = new THREE.Line( geometry, material );
geometry.dynamic = true;
scene.add( line );

scene.background = new THREE.Color( 0xFFFFFF );
function updatePoints(){

    const points = [];
    for (i=0 ; i < range ; i+=resolution){
        points.push( new THREE.Vector3( waveLength*i-startPoint, amplitude*Math.sin(lambda*i + phase), 0 ) );
    }
    geometry.vertices = points
    geometry.verticesNeedUpdate = true;
}

//updateLine(line)
function animate() {
    phase -= waveVelocity
    amplitude = document.getElementById("Amplitude").value
    lambda = document.getElementById("Lambda").value


    updatePoints()
    
	

    requestAnimationFrame(animate)
	renderer.render( scene, camera );
}
animate();


